#include <stdio.h>
#include <pthread.h>

#include "list.h"

list_head_t *list_head;

void* fn1(void *args)
{
  LIST_ADD_KEY(list_head, 1, 0);
  LIST_ADD_KEY(list_head, 2, 0);
  LIST_ADD_KEY(list_head, 300, 0);
}

void* fn2(void *args)
{
  LIST_ADD_KEY(list_head, 4, 0);
  LIST_ADD_KEY(list_head, 5, 0);
  LIST_ADD_KEY(list_head, 600, 0);
}

int main(int argc, char *argv[])
{
  pthread_t pthr1, pthr2;
  int temp;

  LIST_INIT(&list_head);
  
  pthread_create(&pthr1, NULL, fn1, NULL);
  pthread_create(&pthr2, NULL, fn2, NULL);

  pthread_join(pthr1, NULL);
  pthread_join(pthr2, NULL);

  while (1) {
    temp = (int)DEQUEUE(list_head);

    if (temp == NULL)
      break;

    printf("value is %d\n", temp);
  }

  LIST_DEINIT(&list_head);

  return 0;
}
