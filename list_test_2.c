#include <stdio.h>
#include "list.h"

int main(int argc, char *argv[], char *envp[])
{
  int a=1,b=1000, c=1003, *temp;

  list_head_t *list_head, *list_next, *list_pres;

  LIST_INIT(&list_head);

  printf("list intialised! elements to be added\n");
  
  LIST_ADD(list_head, &a);
  LIST_ADD(list_head, &b);
  LIST_ADD(list_head, &c);

  printf("elements added in the list! now retrieving back\n");

  list_pres = list_head;

  while (1) {
    temp = DEQUEUE(list_pres);

    if (temp == NULL)
      break;

    printf("value is %d\n", *temp);
  }

  LIST_DEINIT(&list_head);

  printf("done!\n");

  return 0;
}
