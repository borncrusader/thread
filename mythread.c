#include <sys/syscall.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>

#include "mythread.h"
#include "list.h"

#define CLONE_FLAGS (CLONE_VM|CLONE_FILES|CLONE_SIGHAND|CLONE_FS|CLONE_IO)

/* type definitions */
typedef struct futex futex_t;
typedef void* (*thread_func)(void *);
typedef void (*key_destructor)(void *);

/* thread control block */
typedef struct thread_cb
{
  mythread_t thread_id; // thread_id, same as the thread_id returned by clone

  thread_func func;     // function which needs to be called from wrapper function
  void *args;           // argument for the function

  void *stack;          // use to free

  int join_id;          // id of the thread that this thread wants to join
  void *join_ret;       // return status of the thread that the thread is waiting for

  futex_t thread_futex; // futex for which this thread waits

  list_head_t *key_list_head; // head of the list that maintains all key-value pairs   
}thread_cb_t;

// Linked list node for key value pairs
typedef struct key_val_struc
{
  key_destructor destruc_func; 
  void* key_value; 
}thread_key;

/* globals */
static int key_number = 0;
static thread_cb_t* g_idle_thread_cb;
static thread_cb_t* g_main_thread_cb;
static list_head_t* tcb_head = NULL;

char buf[100];

/* function definitions from here */
/* get the pid of a calling thread */
static pid_t gettid(void)
{
  return (pid_t) syscall(SYS_gettid);
}

/* thread safe function that prints a string */ 
void print(char* str)
{
	int len = strlen(str);
 	write(1, str, len); 
}

/* create stack for the thread
 * aligns the stack to a 64 bit boundary
 * and ensures that the stack size is not less than
 * the sizeof(sigset_t)
 */
void* create_stack(mythread_attr_t *attr ,void **oldptr)
{
  size_t stacksize = SIGSTKSZ;
  size_t basesize = sizeof(sigset_t);
  void *ptr;

  if(attr != NULL) {
    if(attr->stack_size > basesize) {
      stacksize = attr->stack_size;
    } else {
      stacksize = basesize;
    }
  }

  ptr = malloc(stacksize + 63);
  if(ptr == NULL) {
    *oldptr = NULL;
    return NULL;
  }

  *oldptr = (void*)ptr;
  ptr = (void*)((long)(ptr+64) & (long)(~0x3F));

  return ptr;
}

/* A function that picks and runs the next runnable thread */
void schedule(int from_exit)
{
	thread_cb_t *thread_first = NULL, *next_thread = NULL;
	list_element_t *first_elem = NULL, *next_elem = NULL;

	first_elem = LIST_GET_FIRST(tcb_head);
	if(first_elem  !=  NULL)
	{
    // if not called from exit, we can dequeue and enqueue the tcb back
		if(from_exit == 0) 
		{
			thread_first = DEQUEUE(tcb_head);
			LIST_ADD_KEY(tcb_head, thread_first, thread_first->thread_id);
		}

		// find the next element in the list which is not waiting for any
    // other thread to finish (on join) and up its futex
		next_elem = LIST_GET_FIRST(tcb_head); 
		while(next_elem != NULL)
		{
			next_thread = next_elem->element;			
			if(next_thread->join_id != 0) 
			{ 
				DEQUEUE(tcb_head);
				LIST_ADD_KEY(tcb_head, next_thread, next_thread->thread_id);
				next_elem = LIST_GET_FIRST(tcb_head);
				continue;
			}
      break;
		}
		if(next_elem != NULL)
		{
      //sprintf(buf, "scheduling %d\n", next_thread->thread_id);
      //print(buf);
			futex_up(&(next_thread->thread_futex));	 
		} else {
      // shouldn't happen ideally!!
      print("next elem NULL\n");
    }
	}
}

/* idle thread that just yields its control to next thread in the queue */
int idle_thread_func(void* args)
{
  thread_cb_t *thread_cb = args;

  thread_cb->thread_id = gettid();
  LIST_ADD_KEY(tcb_head, thread_cb, thread_cb->thread_id);

  while(1)
  {
	  futex_down(&(thread_cb->thread_futex));
    if(LIST_LENGTH(tcb_head) == 1) {
      //print("idle exiting\n");
      free(thread_cb->stack);
      free(thread_cb);
      futex_up(&(g_main_thread_cb->thread_futex));
      break;
    }
	  schedule(0);
  }

  exit(0);
}

/* initialize the idle thread: create a tcb for it, add it to the list and clone it */
void init_idle_thread()
{
  thread_cb_t*  idle_thread_cb = NULL;
  void *child_stack, *stack_aligned;
  int tid;

  idle_thread_cb = (thread_cb_t*) malloc(sizeof(thread_cb_t)) ; 
  if(idle_thread_cb == NULL) {
    print("malloc failed for idle!");
    exit(1);
  }

  futex_init(&(idle_thread_cb->thread_futex), 0);

  // clone the idle thread so that it starts running
  stack_aligned = create_stack(NULL, &child_stack);
  if(child_stack == NULL)
  {
    print("malloc of child_stack failed in idle thread\n"); 
    exit(1);
  }

  idle_thread_cb->func = (void*) idle_thread_func;
  idle_thread_cb->args = NULL;
  idle_thread_cb->stack = child_stack;
  idle_thread_cb->join_id = 0;
  idle_thread_cb->join_ret = NULL;
  idle_thread_cb->key_list_head = NULL;

  g_idle_thread_cb = idle_thread_cb;

  tid = clone(idle_thread_func, child_stack + SIGSTKSZ, CLONE_FLAGS, idle_thread_cb);
  if(tid == -1) {
    print("clone failed for idle!");
    exit(1);
  }

  return;
}

/* It just creates a cb for main thread, inits its futex and adds it to the end of list */
void init_main_thread()
{
   thread_cb_t*  main_thread_cb = NULL;

   main_thread_cb = (thread_cb_t*) malloc( sizeof(thread_cb_t)); 
   if(main_thread_cb == NULL) {
     print("malloc failed for main!\n");
     exit(1);
   }

   futex_init(&(main_thread_cb->thread_futex), 0);

   main_thread_cb->thread_id = gettid(); 
   main_thread_cb->func = NULL;
   main_thread_cb->args = NULL;
   main_thread_cb->stack = NULL;
   main_thread_cb->join_id = 0;
   main_thread_cb->join_ret = NULL;

   LIST_INIT(&(main_thread_cb->key_list_head));

   LIST_ADD_KEY(tcb_head, main_thread_cb, main_thread_cb->thread_id);

   g_main_thread_cb = main_thread_cb;

   return;
}   

// new thread will wait on futex if there is a thread that is already executing
// but the first thread being created is an exception
static int wrapper_function(void* args)
{
  thread_cb_t* tcb = (thread_cb_t*) args;
  void *ret = NULL;

  // init futex of this thread
  futex_init(&(tcb->thread_futex), 0);
  
  tcb->thread_id = gettid(); 
 
  // enqueue this tcb to the list
  list_element_t *first_elem = LIST_GET_FIRST(tcb_head);
  thread_cb_t* first = first_elem->element;

  // if this is the first in the list, proceed to call the function
  // first created thread, schedule it right away
  if(first->thread_id == tcb->thread_id)
  {
    ret = tcb->func(tcb->args);
  }
  else
  {  
    // if not, futex_down and wait!
    futex_down(&(tcb->thread_futex));
    ret = tcb->func(tcb->args);
  } 

  mythread_exit(ret);
}

/* returns the mythread_t of the current running thread */
mythread_t mythread_self(void)
{
  list_element_t *first_elem = LIST_GET_FIRST(tcb_head);

  if(first_elem == NULL)
    return -1;

  thread_cb_t* tcb = first_elem->element;

  if (tcb->thread_id == gettid())
  {
    return tcb->thread_id;
  }
  // for debugging??
  return -1;
}

/* create a new thread */ 
int mythread_create(mythread_t *mythread_id,
            		    mythread_attr_t *attr,
            		    void * (*thread_func)(void *),
            		    void *args)
{
  void *stack_aligned, *child_stack;
  thread_cb_t* tcb;
  int tid, tret;
  int main_wait = 0;
  size_t stack_size = SIGSTKSZ;
  static int once = 0;

  // create stack, do NULL checking!
  stack_aligned = create_stack(attr, &child_stack);
  if( child_stack == NULL)
  {
     print("malloc of child_stack failed\n"); 
     return -1;
  }
  
  // malloc the thread_cb for this thread, do NULL checking!
  tcb = (thread_cb_t*) malloc(sizeof(thread_cb_t));
  if( tcb == NULL) 
  {
     return -1;
  }
  
  tcb->func = thread_func;
  tcb->args = args;
  tcb->stack = child_stack;
  tcb->join_id = 0;
  tcb->thread_id = 0; /* initialize the tid to be zero */
  LIST_INIT(&(tcb->key_list_head));
  
  // test if the list is null and init 
  if( tcb_head == NULL)
  {
    LIST_INIT(&tcb_head);
  }

  // If this is the first call to mythread_create() - clone an idle thread 
  // and add the newly created thread at the head of the list
  if( once == 0 && g_main_thread_cb == NULL)
  {
     init_main_thread();
     init_idle_thread();
     //print("pushing front\n");
     tret = LIST_PUSH_FRONT(tcb_head, tcb, tcb->thread_id);
     main_wait = 1;
     once = 1;
     // tret = LIST_ADD_KEY(tcb_head, tcb, tcb->thread_id);
  }
  else
  {
     //print("adding back\n");
      tret = LIST_ADD_KEY(tcb_head, tcb, tcb->thread_id);

      // Add existing key list to this new thread
      list_head_t *head;
      list_element_t *elem;
      head = g_main_thread_cb->key_list_head;
      elem = LIST_GET_FIRST(head);
      thread_key *new_struc;
       
      while(elem != NULL)
      {
        new_struc = (thread_key *)malloc(sizeof(thread_key));
        if(new_struc != NULL)
        {  
          new_struc->key_value = NULL;
          new_struc->destruc_func = NULL;  
          LIST_ADD_KEY(tcb->key_list_head, new_struc, elem->key);
          elem = LIST_GET_NEXT(elem);
        }
      }
  }

  if(attr != NULL) {
    stack_size = attr->stack_size;
  }

  // do clone and check clone's return value!
  tid = clone(wrapper_function,
              stack_aligned + stack_size,
              CLONE_FLAGS, tcb);

  
  if( tid == -1)
	return -1; 
  *mythread_id = tid;	
  tcb->thread_id = tid;
  if(main_wait == 1)
  {
  //print("setting once to 1\n");
	// main thread waits here until the newly created thread yields
	futex_down(&(g_main_thread_cb->thread_futex));
  }

  return 0;
}

/* yield control to another thread and halt the calling thread*/
int mythread_yield(void)
{
  list_element_t* first_elem = LIST_GET_FIRST(tcb_head);
  thread_cb_t* running_thread = first_elem->element;

  schedule(0); 
  
  futex_down(&(running_thread->thread_futex));
}

/* wait for the target_thread to exit */
int mythread_join(mythread_t target_thread, void **status)
{
  list_element_t* first_elem;
  thread_cb_t* first_thread, *target_tcb;

  first_elem = LIST_GET_FIRST(tcb_head);
  first_thread = first_elem->element;

  target_tcb = LIST_GET_KEY(tcb_head, target_thread);
  if(target_tcb == NULL) {
    return -1;
  }
  
  first_thread->join_id = target_thread; 
  schedule(0); 
  futex_down(&(first_thread->thread_futex));  

  if( status != NULL )
    *status = first_thread->join_ret;

  return 0;
}

/* 1. If exit is called from main, it must wait for all the threads.
   2. If exit is already called by the user code, it should have no effect when called again in wrapper code
   3. Clean : stack, List Node */
void mythread_exit(void *retval)
{
  thread_cb_t *running_thread, *temp_thread;
  list_element_t *temp = NULL;

  running_thread = DEQUEUE(tcb_head);

  if(running_thread == g_main_thread_cb) {
    // if num threads is > 1 (only the idle thread), then wait for all the buggers
    if(LIST_LENGTH(tcb_head) > 1) {
      // set join id as -1
      running_thread->join_id = -1;

      // we append this thread back to the queue!
      LIST_ADD_KEY(tcb_head,running_thread, running_thread->thread_id);
 
      // schedule the next thread
      schedule(1);

      // wait on my futex
      //sprintf(buf, "main going down on futex %d\n", gettid());
     // print(buf);
      futex_down(&(running_thread->thread_futex));
      //print("main recvd futex!\n");

      DEQUEUE(tcb_head);
    } else {
      //print("main happily exiting!\n");
    }

    // idle thread should hopefully be running still, let me up him
    //futex_up(&(g_idle_thread_cb->thread_futex));
    schedule(1);
    // let me wait for idle to exit!
    futex_down(&(running_thread->thread_futex));

    LIST_DEINIT(&tcb_head);
    // else can happily exit
  } else {
    // normal thread
    // unset the join id of the threads waiting for this thread
    temp = LIST_GET_FIRST(tcb_head);
    while(temp != NULL)
    {
      thread_cb_t* temp_thread = (thread_cb_t*)temp->element;
      if(temp_thread->join_id == running_thread->thread_id)
      {
        temp_thread->join_id = 0;
        temp_thread->join_ret = retval; 
      }
      temp = LIST_GET_NEXT(temp); 
    }

    // if last thread to exit, unset the join id of main too
    if(LIST_LENGTH(tcb_head) == 2) {	
      g_main_thread_cb->join_id = 0; // to make sure that main becomes schedulable
    }

    // schedule the next thread
    schedule(1);
  }

  // clean keylist and call destructors
  thread_key* key = NULL;
  list_head_t* keylist = running_thread->key_list_head; 
  list_element_t* list = LIST_GET_FIRST(keylist);

  while(list != NULL)
  {
    key = DEQUEUE(keylist); 
    if(key->destruc_func != NULL)
      key->destruc_func(key->key_value);
    free(key); 
    list = LIST_GET_FIRST(keylist); 
  }
  LIST_DEINIT(&(running_thread->key_list_head));

  free(running_thread->stack); 
  //free(running_thread);

  exit(0);
}


int mythread_key_create(mythread_key_t *key, void (*destructor)(void*))
{
  list_element_t *list_itr;
  thread_cb_t* tcb_itr;
  thread_key* key_struc; 
  int ret; 

  list_itr = NULL;
  tcb_itr = NULL;

  list_itr = LIST_GET_FIRST(tcb_head);

  // Add this key to all the available threads in the queue
  while(list_itr != NULL)
  {
    tcb_itr = list_itr->element;

    // If the current thread is not idle, then add the key
    if(tcb_itr != g_idle_thread_cb) 
    {
      key_struc = (thread_key *)malloc(sizeof(thread_key));

      // If memory allocation is successful, add the key
      if(key_struc != NULL)
      {   
        key_struc->key_value = NULL;
        key_struc->destruc_func = destructor;
        *key = key_number;
        tcb_itr = list_itr->element;
        ret = LIST_ADD_KEY(tcb_itr->key_list_head, key_struc, *key);

      //printf("list head is now %x of %d\n", tcb_itr->key_list_head, tcb_itr->thread_id);
        // If list add operation fails, return error
        if(ret) {
          printf("%d list add key error!\n", ret);
          return -1; 
        }
      }
      else
      {
        return -2;  
      }
    }
    list_itr = LIST_GET_NEXT(list_itr);        
  }

  key_number = key_number + 1;
  return 0;
}

void* mythread_getspecific(mythread_key_t key)
{
  list_element_t *list_itr;
  thread_cb_t* tcb_itr;
  thread_key* key_struc;

  list_itr = NULL;
  tcb_itr = NULL;
  key_struc = NULL;
  list_itr = LIST_GET_FIRST(tcb_head);
  tcb_itr = list_itr->element;
  key_struc = (void *) LIST_GET_KEY(tcb_itr->key_list_head, key);  
  //char *get = (char *)key_struc->key_value;
  if(key_struc != NULL)
    return key_struc->key_value; 
  else
    return NULL; 
}


int mythread_setspecific(mythread_key_t key, const void *value)
{
  list_element_t *list_itr; 
  thread_cb_t* tcb_itr;
  thread_key* key_struc;

  list_itr = NULL;
  tcb_itr = NULL;
  list_itr = LIST_GET_FIRST(tcb_head);
  tcb_itr = list_itr->element;
  key_struc = (void *) LIST_GET_KEY(tcb_itr->key_list_head, key);    
  if(key_struc != NULL) 
  {   
    key_struc->key_value = (void *) value;
    return 0; 
  }  

  return -1;
}

int mythread_key_delete(mythread_key_t key)
{
  list_element_t *list_itr;
  thread_cb_t* tcb_itr;
  thread_key* key_struc;

  list_itr = NULL;
  tcb_itr = NULL;

  list_itr = LIST_GET_FIRST(tcb_head);
  while(list_itr)
  {
    tcb_itr = list_itr->element;
    if(tcb_itr != g_idle_thread_cb) {
      key_struc = (void *) LIST_DELETE_KEY(tcb_itr->key_list_head, key);
      if(key_struc == NULL) {
        return EINVAL;
      }
      free(key_struc);
    }
    //DEQUEUE(tcb_itr->key_list_head);
    list_itr = LIST_GET_NEXT(list_itr);  
  }

  return 0;
}
