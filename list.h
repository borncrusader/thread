#ifndef __LIST_H__
#define __LIST_H__

//#include "futex.h"

/* functions
 * LIST_INIT         - call when first creating a list
 *                     will malloc the list_head for use by the API
 * LIST_ELEMENT_INIT - initialises the element to NULL
 * LIST_ADD          - adds the element to the last of the list with key as 0
 * LIST_ADD_KEY      - adds the element to the last of the list with specified key
 * LIST_DELETE_KEY   - removes the element with the specified key
 * LIST_GET_KEY      - gets the element with the specified key
 * LIST_GET_FIRST    - gets the first list_element of the list
 * LIST_GET_NEXT     - gets the list_element next to the specified element
 * LIST_GET_PREV     - gets the list_element previous to the specified element
 * LIST_LENGTH       - returns the length of the list
 * DEQUEUE           - removes the first element and returns it
 * ENQUEUE           - adds the element to the back of the list
 * LIST_DEINIT       - call when the list is done with
 *                     will free everything
 *                     not responsible for freeing the individual elements
 *
 * working
 * - most functions will return 0 on TRUE and non-zero on ERROR
 */

typedef struct list_element
{
  int                  key;
  void                *element;

  struct list_element *next;
  struct list_element *prev;
}list_element_t;

typedef struct list_head
{
  int length;
  //struct futex list_futex;

  struct list_element *first;
  struct list_element *last;
}list_head_t;

int LIST_INIT(list_head_t **list_head);
int LIST_ELEMENT_INIT(list_element_t *list_elem);
int LIST_ADD_KEY(list_head_t *list_head, void *list_elem, int key);
int LIST_PUSH_FRONT(list_head_t *list_head, void *list_elem, int key);
void* LIST_GET_KEY(list_head_t *list_head, int key);
int LIST_CHANGE_KEY(list_head_t *list_head, int key, void *value);
list_element_t* LIST_GET_FIRST(list_head_t *list_head);
list_element_t* LIST_GET_NEXT(list_element_t *list_elem);
list_element_t* LIST_GET_PREV(list_element_t *list_elem);
void* LIST_DELETE_KEY(list_head_t *list_head, int key);
void* DEQUEUE(list_head_t *list_head);
int LIST_DEINIT(list_head_t **list_head);

#define LIST_LENGTH(_x) (_x)->length

#define ENQUEUE(_x,_y,_z) LIST_ADD_KEY(_x,(void*)_y,_z)
#define LIST_ADD(_x,_y) LIST_ADD_KEY(_x,_y,0)

//#define LIST_ADD_KEY(_x,_y,_z) _LIST_ADD_KEY(_x,(void*)_y,_z)
//#define LIST_GET_KEY(_x,_y,_z) (_x*)_LIST_GET_KEY(_y,_z)
//#define LIST_DELETE_KEY(_x,_y,_z) (_x*)_LIST_DELETE_KEY(_y,_z)
//#define DEQUEUE(_x,_y) (_x*)(_DEQUEUE(_y))

#endif
