CC     = gcc
CFLAGS = -g

thread_test: test.o mythread.o futex.o list.o
	$(CC) $(CFLAGS) -o thread_test test.o mythread.o futex.o list.o

mythread: mythread.c
	$(CC) $(CFLAGS) -c mythread.c 

test: test1.c
	$(CC) $(CFLAGS) -c test1.c 

list.o: list.c
	$(CC) $(CFLAGS) -c list.c

futex.o: futex.c
	$(CC) $(CFLAGS) -c futex.c

list_test: list.o futex.o
	$(CC) $(CFLAGS) list_test_1.c list.o futex.o -o list_test_1
	$(CC) $(CFLAGS) list_test_2.c list.o futex.o -o list_test_2

clean:
	rm -rf *.o
	rm -rf thread_test list_test_1 list_test_2

