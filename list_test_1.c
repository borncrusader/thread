#include <stdio.h>
#include "list.h"

typedef struct {
  int i;
  char c;
  float f;
}Numbers;

int main(int argc, char *argv[], char *envp[])
{
  Numbers a={1,'a',1.0}, b={2,'b',2.0} ,c={3,'c',3.0}, *temp = NULL;
  
  list_head_t *list_head, *list_next, *list_pres;

  LIST_INIT(&list_head);

  printf("list intialised! elements to be added\n");
  
  LIST_ADD_KEY(list_head, &a, 10);
  LIST_ADD_KEY(list_head, &b, 13);
  LIST_ADD_KEY(list_head, &c, 16);

  printf("elements added in the list! now retrieving back\n");

  list_pres = list_head;

  temp = LIST_GET_KEY(list_head, 16);
  if(temp)
    printf("values are %d %c %f\n", temp->i, temp->c, temp->f);

  while (1) {
    temp = DEQUEUE(list_pres);

    if (temp == NULL)
      break;

    printf("values are %d %c %f\n", temp->i, temp->c, temp->f);
  }

  printf("done!\n");

  return 0;
}
