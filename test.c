#include <fcntl.h>
#include <sys/syscall.h>
#include "mythread.h"
#include <stdio.h>
#include <stdlib.h>
int variable;   

static pid_t gettid(void)
{
  return (pid_t) syscall(SYS_gettid);
}

void* third_front(void* args)
{
  char buf[100];

	sprintf(buf, "third function now runs:%d\n",(int) args);
	print(buf);
	mythread_yield();
	sprintf(buf, "third function quits");
	print(buf);
}

void* dummy_function(void* args)
{
  char buf[100];

  sprintf(buf, "\ngoing to yield - dummy func\n");
  print(buf);
  mythread_yield();
  sprintf(buf, "\nDummy func value:%d\n",gettid());
  print(buf);
  mythread_yield();
  mythread_t tid[2];
  int i ;

  for( i = 0;i< 1;i++)
  {
    mythread_create(&tid[i],NULL, third_front, (void*)i); 
    sprintf(buf, "going to wait in dummy func\n");
    print(buf);
    mythread_join(tid[i], NULL);
  }
  sprintf(buf, "waited on join successfully, main should exit now!\n");
  print(buf);
  mythread_exit(NULL);
  sprintf(buf, "\nDummy function exit!");
  print(buf);
}

void* do_something2(void* args)
{
char buf[100];
  sprintf(buf, "do something2: %d, %d\n", (int)args, mythread_self());
  print(buf);

  //mythread_exit(NULL);
}

void* do_something1(void* args)
{
  mythread_t tid[10];
  int i = 0;
char buf[100];

  sprintf(buf, "do something1: %d, %d\n", (int)args, mythread_self());
  print(buf);

  for(i=0; i<10; i++) {
    if(mythread_create(&tid[i], NULL, do_something2, (void*)i) == -1) {
      print("no memory!\n");
    }
    print("spawning!\n");
    mythread_join(tid[i], NULL);
  }

  //for(i=0; i<10; i++)
    //mythread_join(&tid[i], NULL);

 // mythread_exit(NULL);
}

int main(int argc, char *argv[])
{
  mythread_t tid[100];
  int i = 0;
char buf[100];

  sprintf(buf, "main: %d\n", gettid());
  print(buf);

  for(i=0; i<51; i++) {
    if(mythread_create(&tid[i], NULL, do_something1, (void*)i) == -1) {
      print("no memory!\n");
    }
    mythread_join(tid[i], NULL);
  }

  mythread_exit(NULL);

  return 0;
}
