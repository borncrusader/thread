#include <stdlib.h>
#include "list.h"

int LIST_INIT(list_head_t **list_head)
{
  if(list_head == NULL) {
    return 1;
  }

  *list_head = (list_head_t*)malloc(sizeof(list_head_t));

  if(!(*list_head)) {
    return 2;
  }

  (*list_head)->length = 0;
  //futex_init(&((*list_head)->list_futex), 1);

  (*list_head)->first = NULL;
  (*list_head)->last = NULL;

  return 0;
}

int LIST_ELEMENT_INIT(list_element_t *list_elem)
{
  if(list_elem == NULL) {
    return 1;
  }

  list_elem->key = 0;
  list_elem->element = NULL;

  list_elem->next = NULL;
  list_elem->prev = NULL;

  return 0;
}

int LIST_ADD_KEY(list_head_t *list_head, void *list_elem, int key)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *c_list_elem = NULL;

  if(!list_head) {
    return 1;
  }

  c_list_elem = (list_element_t*) malloc(sizeof(list_element_t));
  if(!c_list_elem) {
    return 2;
  }

  LIST_ELEMENT_INIT(c_list_elem);
  
  c_list_elem->key = key;
  c_list_elem->element = list_elem;

  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;

  if(!t_list_elem) {
    list_head->first = c_list_elem;
  } else {
    while(t_list_elem && t_list_elem->next != NULL) {
      t_list_elem = t_list_elem->next;
    }
    t_list_elem->next = c_list_elem;
    c_list_elem->prev = t_list_elem;
  }

  list_head->length++;
  list_head->last = c_list_elem;

  //futex_up(&(list_head->list_futex));

  return 0;
}

int LIST_CHANGE_KEY(list_head_t *list_head, int key, void *value)
{
  list_element_t *t_list_elem = NULL;

  if(!list_head) {
    return 1;
  }

  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;

  while(t_list_elem) {
    if(t_list_elem->key == key) {
      t_list_elem->element = value;
      //futex_up(&(list_head->list_futex));
      return 0;
    }
    t_list_elem = t_list_elem->next;
  }

  //futex_up(&(list_head->list_futex));
 
  return 1;
}

int LIST_PUSH_FRONT(list_head_t *list_head, void *list_elem, int key)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *c_list_elem = NULL;

  if(!list_head) {
    return 1;
  }

  c_list_elem = (list_element_t*) malloc(sizeof(list_element_t));
  if(!c_list_elem) {
    return 2;
  }

  LIST_ELEMENT_INIT(c_list_elem);
  
  c_list_elem->key = key;
  c_list_elem->element = list_elem;

  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;
  c_list_elem->next = t_list_elem;
  if(t_list_elem) {
    t_list_elem->prev = c_list_elem;
  }

  list_head->length++;
  list_head->first = c_list_elem;

  //futex_up(&(list_head->list_futex));

  return 0;
}

void* LIST_GET_KEY(list_head_t *list_head, int key)
{
  list_element_t *t_list_elem = NULL;
  void *element = NULL;

  if(!list_head) {
    return NULL;
  }
  
  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;

  while(t_list_elem) {
    if(t_list_elem->key == key) {
      element = t_list_elem->element;
      break;
    }
    t_list_elem = t_list_elem->next;
  }

  //futex_up(&(list_head->list_futex));

  return element;
}

void* LIST_DELETE_KEY(list_head_t *list_head, int key)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *prev_elem, *next_elem;
  void *element = NULL;

  if(!list_head) {
    return NULL;
  }

  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;

  while(t_list_elem) {
    if(t_list_elem->key == key) {
      element = t_list_elem->element;

      prev_elem = t_list_elem->prev;
      next_elem = t_list_elem->next;

      if(prev_elem) {
        prev_elem->next = next_elem;
      } else {
        // first element is removed
        list_head->first = next_elem;
      }
      if(next_elem) {
        next_elem->prev = prev_elem;
      } else {
        // last element is removed
        list_head->last = prev_elem;
      }

      free(t_list_elem);

      list_head->length--;
      break;
    }

    t_list_elem = t_list_elem->next;
  }

  //futex_up(&(list_head->list_futex));

  return element;
}

void* DEQUEUE(list_head_t *list_head)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *prev_elem, *next_elem;
  void *element = NULL;

  if(!list_head) {
    return NULL;
  }

  //futex_down(&(list_head->list_futex));

  t_list_elem = list_head->first;

  if(t_list_elem) {
    element = t_list_elem->element;

    prev_elem = t_list_elem->prev;
    next_elem = t_list_elem->next;

    if(prev_elem) {
      prev_elem->next = next_elem;
    } else {
      // first element is removed
      list_head->first = next_elem;
    }
    if(next_elem) {
      next_elem->prev = prev_elem;
    } else {
      // last element is removed
      list_head->last = prev_elem;
    }

    free(t_list_elem);

    list_head->length--;
  }

  //futex_up(&(list_head->list_futex));

  return element;
}

list_element_t* LIST_GET_FIRST(list_head_t *list_head)
{
  if(!list_head) {
    return NULL;
  }

  return list_head->first;
}

list_element_t* LIST_GET_NEXT(list_element_t *list_elem)
{
  if(!list_elem) {
    return NULL;
  }

  return list_elem->next;
}

list_element_t* LIST_GET_PREV(list_element_t *list_elem)
{
  if(!list_elem) {
    return NULL;
  }

  return list_elem->prev;
}

int LIST_DEINIT(list_head_t **list_head)
{
  if(!list_head || !*list_head) {
    return 1;
  }

  if((*list_head)->length > 0) {
    /* list has atleast one element! */
    return 2;
  }

  free(*list_head);
  *list_head = NULL;

  return 0;
}
